vertx-test-frontend

For launch:
-go to project folder;
-'npm install' in console;
-'npm run build' in console;
-'npm run start' in another console;
-open 'localhost:4000' in browser.

links to screens: 
'/registration-form' - форма регистрации
'/login-form' - форма логина
'/user-cabinet' - главная страница (доступна после авторизации пользователя)