var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var mongoose = require('mongoose');
// var MongoStore = require('connect-mongo');


mongoose.connect("mongodb://localhost:27017/OMUT", {useNewUrlParser: true});
var db = mongoose.connection;

var app = express();


app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(__dirname));

app.post('/api/signup', (req, res, next) => {

    let userCredentials = {
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    };
    db.collection('users').insert(userCredentials, (err) => {
        if (err) {
            res.status(err.status).json({
                message: err.message
            });
        } else {
            res.status(200).json({
                message: 'Пользователь зарегистрирован'
            });
        }
    })

});

app.post('/api/login', (req, res, next) => {

    let userCredentials = {
        email: req.body.email,
        password: req.body.password
    };

    db.collection("users").findOne(
        {
            email: req.body.email,
            password: req.body.password
        },
        (err, doc) => {
            console.dir(doc);
            if(doc !== null) {
                res.status(200).json({
                    message: 'Пользователь вошёл',
                    username: doc.username
                });
            } else {
                console.log(err);
                res.status(401).json({
                    message: "Нет такого пользователя"
                });
            }
        })
});

app.post('/api/submit-form', (req, res, next) => {
    let userData = {
        "Family": req.body.Family,
        "Name": req.body.Name,
        "FName": req.body.FName,
        "BYear": req.body.BYear,
        "City": req.body.City || "Пусто",
    };
    db.collection('usersData').insert(userData, (err) => {
        if (err) {
            res.status(err.status).json({
                message: err.message
            });
        } else {
            res.status(200).json({
                message: 'Данные приняты'
            });
        }
    })
});

// app.post('/get-user-data', (req, res) => {
//     let email = req.body.email;
//     let userData = db.findOne("");
// });

app.get('*', (req, res) => {

    res.sendFile(path.resolve(__dirname, 'index.html'));

});


app.listen(4000, () => {
    console.info("App listening on port 4000");
});