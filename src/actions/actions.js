
/*User*/

export const userIsLogged = () => {
    return {
        type: 'USER_IS_LOGGED'
    }
};

export const userIsLogouted = () => {
    return {
        type: 'USER_IS_LOGOUTED'
    }
};

/*Forms*/

export const showConfirmForm = () => {
    return {
        type: 'SHOW_CONFIRM_FORM'
    }
};

export const closeConfirmForm = () => {
    return {
        type: 'CLOSE_CONFIRM_FORM'
    }
};

export const showForgotPasswordForm = () => {
    return {
        type: 'SHOW_FORGOT_PASSWORD_FORM'
    }
};

export const closeForgotPasswordForm = () => {
    return {
        type: 'CLOSE_FORGOT_PASSWORD_FORM'
    }
};

export const showForm = () => {
    return {
        type: 'SHOW_FORM'
    }
};

export const setUsername = (username) => {
    return {
        type: 'SET_USERNAME',
        payload: username
    }
};


/*Cabinet*/

export const setCurrentPathname = (pathname) => {
    return {
        type: 'SET_CURRENT_PATHNAME',
        payload: pathname
    }
};

export const getAccessToViewAndEdit = () => {
    return {
        type: 'GET_ACCESS_TO_VIEW_AND_EDIT'
    }
};