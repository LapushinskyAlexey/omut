import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Breadcrumb} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import * as actions from '../../actions/actions';


class Breadcrumbs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbsArray: []
        };
    };

    static getDerivedStateFromProps(props, state) {
        let {currentPathname} = props;
        let breadcrumbsArray = currentPathname.substr(1).split("/");

        return {breadcrumbsArray: breadcrumbsArray};
    };

    mapBreadcrumbsArray = () => {
        let {breadcrumbsArray} = this.state;

        return breadcrumbsArray.map((item, key) => {
            let newUrl = breadcrumbsArray.slice(0, ++key).join('/');

            return (
                <span key={key}><Link to={`/${newUrl}`}>{item}</Link> / </span>)
        });

    };

    render() {

        let crumbs = this.mapBreadcrumbsArray();

        return (
            <div>
                <Breadcrumb>
                    {crumbs}
                </Breadcrumb>
            </div>
        );
    }
}

const MapStateToProps = (state) => {
    return {
        currentPathname: state.currentPathname
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

// export default connect(MapStateToProps, mapDispatchToProps)(CabinetHeader);
export default connect(MapStateToProps, mapDispatchToProps)(Breadcrumbs);