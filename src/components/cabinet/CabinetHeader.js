import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import axios from 'axios';
import {Link} from 'react-router-dom';

import Breadcrumbs from './Breadcrumbs'
import * as actions from '../../actions/actions';


class CabinetHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            accessToViewAndEdit: false
        };
    };

    static getDerivedStateFromProps(props, state) {
        let {setUsername, getAccessToViewAndEdit} = props.actions;
        let username = localStorage.getItem("username");
        let accessToViewAndEdit = JSON.parse(localStorage.getItem("accessToViewAndEdit")) || false;
        if(accessToViewAndEdit) {
            getAccessToViewAndEdit();
        }
        return {
            username: username,
            accessToViewAndEdit: accessToViewAndEdit
        };
    };

    componentDidMount() {
        const {username} = this.props;
        this.setState({username: username});
    };

    toLogout = () => {
        const {userIsLogouted} = this.props.actions;
        userIsLogouted();
        sessionStorage.setItem("userIsLogged", "false");
        localStorage.setItem("accessToViewAndEdit", "false");
        localStorage.setItem("username", "");
        location.href = "/login-form";
    };


    render() {

        const {username} = this.state;
        const {accessToViewAndEdit} = this.props;

        let buttons =
            <Nav pullRight>
                <NavItem eventKey={1} href="#">
                    {username}
                </NavItem>
                <NavItem eventKey={2} onClick={this.toLogout}>
                    <div>Выйти</div>
                </NavItem>
            </Nav>;

        if (accessToViewAndEdit) {
            buttons =
                <Nav pullRight>
                    <Navbar.Brand>
                        <Link to="/user-cabinet/view-page">Посмотреть</Link>
                    </Navbar.Brand>
                    <Navbar.Brand>
                        <Link to="/user-cabinet/edit-page">Редактировать</Link>
                    </Navbar.Brand>
                    <NavItem eventKey={3} href="#">
                        {username}
                    </NavItem>
                    <NavItem eventKey={4} onClick={this.toLogout}>
                        <div>Выйти</div>
                    </NavItem>
                </Nav>
        }

        return (
            <div>
                <Navbar inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link to="/user-cabinet">Главная</Link>
                        </Navbar.Brand>
                        <Navbar.Toggle/>
                    </Navbar.Header>
                    <Navbar.Collapse>
                        {buttons}
                    </Navbar.Collapse>
                </Navbar>
                <Breadcrumbs/>
            </div>
        );
    }
}

const MapStateToProps = (state) => {
    return {
        username: state.username,
        accessToViewAndEdit: state.accessToViewAndEdit
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(MapStateToProps, mapDispatchToProps)(CabinetHeader);