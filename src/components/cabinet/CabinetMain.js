import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import UsersList from './UsersList';
import Home from './Home';
import CabinetHeader from './CabinetHeader';
import ViewPage from './ViewPage';
import EditPage from './EditPage';


export const CabinetMain = () => (
    <BrowserRouter>
        <div>
            <div>
                <CabinetHeader/>
            </div>
            <div>
                <Switch>
                    <Route exact path="/user-cabinet" component={Home}/>
                    <Route exact path="/user-cabinet/view-page" component={ViewPage}/>
                    <Route exact path="/user-cabinet/edit-page" component={EditPage}/>
                </Switch>
            </div>
        </div>
    </BrowserRouter>
);