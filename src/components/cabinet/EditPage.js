import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, FormGroup, Col, FormControl, ControlLabel, Button, Alert} from 'react-bootstrap';
import axios from 'axios';

import * as actions from "../../actions/actions";


class EditPage extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    static getDerivedStateFromProps(props, state) {

        return null;
    }

    render() {

        return (
            <div>
                EditPage
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToViewAndEdit: state.accessToViewAndEdit
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPage);