import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, FormGroup, Col, FormControl, ControlLabel, Button, Alert} from 'react-bootstrap';
import axios from 'axios';

import {TestText} from './styles';
import * as actions from "../../actions/actions";


class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            patronymic: '',
            surname: '',
            yearOfBirth: '',
            cityOfResidence: '',
            alert: {
                status: '',
                message: ''
            },
            textFieldsIsValid: false,
            numberFieldsIsValid: false
        };
    }

    static getDerivedStateFromProps(props, state) {
        const pathname = location.pathname;
        let {setCurrentPathname, userIsLogged} = props.actions;
        setCurrentPathname(pathname);
        userIsLogged();

        return null;
    }

    handleName = (event) => {
        this.setState({name: event.target.value}, () => {
            this.checkTextField();
        });
    };

    handlePatronymic = (event) => {
        this.setState({patronymic: event.target.value}, () => {
            this.checkTextField();
        });
    };

    handleSurname = (event) => {
        this.setState({surname: event.target.value}, () => {
            this.checkTextField();
        });
    };

    handleYearOfBirth = (event) => {
        this.setState({yearOfBirth: event.target.value}, () => {
            this.checkNumberField();
        });
    };

    handleCityOfResidence = (event) => {
        this.setState({cityOfResidence: event.target.value});
    };

    checkTextField = () => {
        let {name, patronymic, surname} = this.state;

        const regexp = /[а-яА-ЯЁё-і]/gi;

        let nameMatches = name.match(regexp);
        let patronymicMatches = patronymic.match(regexp);
        let surnameMatches = surname.match(regexp);

        if ((nameMatches !== null && name.length === nameMatches.length) &&
            (patronymicMatches && patronymic.length === patronymicMatches.length) &&
            (surnameMatches !== null && surname.length === surnameMatches.length)) {
            this.setState({textFieldsIsValid: true});
        } else {
            this.setState({textFieldsIsValid: false});
        }
    };

    checkNumberField = () => {
        let {yearOfBirth} = this.state;

        if (yearOfBirth.length === 4 && typeof(+yearOfBirth) === "number") {
            console.log("numberFieldsIsValid: true");
            this.setState({numberFieldsIsValid: true});
        } else {
            console.log("numberFieldsIsValid: false");
            this.setState({numberFieldsIsValid: false});
        }

    };

    closeAlert = () => {
        this.setState({
            alert: {
                status: '',
                message: ''
            }
        });
    };

    submitForm = (event) => {
        let {
            name,
            patronymic,
            surname,
            yearOfBirth,
            cityOfResidence,
            alert
        } = this.state;

        let {getAccessToViewAndEdit} = this.props.actions;

        let body = {
            Family: surname,
            Name: name,
            FName: patronymic,
            BYear: yearOfBirth,
            City: cityOfResidence
        };

        event.preventDefault();

        axios.post(`/api/submit-form`, body)
            .then((res) => {
                if (res.status !== 200) {
                    console.log(res.status);
                    this.setState({
                        alert: {
                            status: "danger",
                            message: JSON.stringify(res.data.message)
                        }
                    });
                } else {
                    this.setState({
                        name: '',
                        patronymic: '',
                        surname: '',
                        yearOfBirth: '',
                        cityOfResidence: '',
                        alert: {
                            status: "info",
                            message: JSON.stringify(res.data.message)
                        },
                        textFieldsIsValid: false,
                        numberFieldsIsValid: false
                    });
                    getAccessToViewAndEdit();
                    localStorage.setItem("accessToViewAndEdit", "true");
                }
            });
    };

    render() {

        let {
            name,
            patronymic,
            surname,
            yearOfBirth,
            cityOfResidence,
            textFieldsIsValid,
            numberFieldsIsValid,
            alert
        } = this.state;

        let infoMessage = null;

        if (alert.message !== '') {
            infoMessage =
                <Alert bsStyle={`${alert.status}`}>
                    <strong>{alert.message}</strong>
                    <Button onClick={this.closeAlert}>Hide Alert</Button>
                </Alert>
        }

        return (
            <TestText>
                <Form horizontal>

                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            Name
                        </Col>
                        <Col sm={5}>
                            <FormControl type="text" placeholder="John" value={name} required={true}
                                         onChange={this.handleName}/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            Patronymic
                        </Col>
                        <Col sm={5}>
                            <FormControl type="text" placeholder="Alex" value={patronymic} required={true}
                                         onChange={this.handlePatronymic}/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            Surname
                        </Col>
                        <Col sm={5}>
                            <FormControl type="text" placeholder="Smith" value={surname} required={true}
                                         onChange={this.handleSurname}/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            Year of birth
                        </Col>
                        <Col sm={5}>
                            <FormControl type="number" placeholder="1994" value={yearOfBirth} required={true}
                                         onChange={this.handleYearOfBirth}/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            City of residence
                        </Col>
                        <Col sm={5}>
                            <FormControl type="text" placeholder="Paris" value={cityOfResidence}
                                         onChange={this.handleCityOfResidence}/>
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={6} sm={1}>
                            <Button type="submit" disabled={!textFieldsIsValid || !numberFieldsIsValid}
                                    onClick={this.submitForm}>Submit</Button>
                        </Col>
                    </FormGroup>
                </Form>

                {infoMessage}
            </TestText>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToViewAndEdit: state.accessToViewAndEdit
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);