import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import axios from 'axios';
import {Table} from 'react-bootstrap';

import * as actions from "../../actions/actions";


class UserInformation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userInfo: {
                username: 'Leon',
                callingCount: '1',
                email: 'Leon@test.com',
                firstTransaction: '21/08/2008',
                totalCountOfTransaction: '24'
            }
        }
    };

    static getDerivedStateFromProps(props, state) {
        const pathname = location.pathname;
        let {setCurrentPathname} = props.actions;

        setCurrentPathname(pathname);

        return null;
    }

    render() {

        const {username, callingCount, email, firstTransaction, totalCountOfTransaction} = this.state.userInfo;

        return (
            <div>
                <Table responsive>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Total count of API calling</th>
                        <th>Email</th>
                        <th>Date of the first transaction</th>
                        <th>Total count of transactions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{username}</td>
                        <td>{callingCount}</td>
                        <td>{email}</td>
                        <td>{firstTransaction}</td>
                        <td>{totalCountOfTransaction}</td>
                    </tr>
                    </tbody>
                </Table>
            </div>
        );
    }
}

const MapStateToProps = (state) => {
    return {
        username: state.username
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(MapStateToProps, mapDispatchToProps)(UserInformation);