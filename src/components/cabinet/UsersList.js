import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import * as actions from "../../actions/actions";


class UsersList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [
                {
                    username: "Leo",
                    callingCount: 1,
                    email: "leo@test.com"
                },
                {
                    username: "Alex",
                    callingCount: 13,
                    email: "alex@test.com"
                },

                {
                    username: "Valera",
                    callingCount: 8,
                    email: "valera@test.com"
                }
            ]
        }
    };

    static getDerivedStateFromProps(props, state) {
        const pathname = location.pathname;
        let {setCurrentPathname} = props.actions;

        setCurrentPathname(pathname);

        return null;
    }

    toParsedUsers =() => {

        //request for get users here

        const {users} = this.state;

        return users.map((elem, key) => {
            const {username, callingCount, email} = elem;
            return (
                <tr key={key}>
                    <td>{key}</td>
                    <td>
                        <Link to={`/user-cabinet/users-list/user-info-${key}`}>
                            {username}
                        </Link>
                    </td>
                    <td>{callingCount}</td>
                    <td>{email}</td>
                </tr>
            );
        });
    };

    render() {

        let parsedUsers = this.toParsedUsers();

        return (
            <div>
                <Table responsive>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>User name</th>
                        <th>API calling count</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    {parsedUsers}
                    </tbody>
                </Table>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(null, mapDispatchToProps)(UsersList);