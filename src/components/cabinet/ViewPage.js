import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, FormGroup, Col, FormControl, ControlLabel, Button, Alert} from 'react-bootstrap';
import axios from 'axios';

import * as actions from "../../actions/actions";


class ViewPage extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    static getDerivedStateFromProps(props, state) {

        let currentEmail = sessionStorage.getItem("email");

        axios.post('/get-user-data', {
            email: currentEmail
        })
            .then((res) => {
                console.log(res.data);
            });
    }

    render() {

        return (
            <div>
                ViewPage
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToViewAndEdit: state.accessToViewAndEdit
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewPage);