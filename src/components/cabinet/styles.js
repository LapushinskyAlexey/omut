import styled from 'styled-components';


export const Logo = styled.h3`
    color: wheat;
    margin: 12px 0 0 -50px;
`;

export const Logout = styled.div`
    text-decoration: underline;
    font-size: 20px;
    margin-left: 100px;
`;

export const TestText = styled.h1`
    text-align: center;
`;