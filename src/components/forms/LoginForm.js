import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import axios from 'axios';
import {Form, FormGroup, Col, FormControl, Button, ControlLabel, Modal} from 'react-bootstrap';

import * as actions from "../../actions/actions";


class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            formIsValid: false,
            formIsShowing: true
        };
    }

    handleEmailChanging = (event) => {
        this.setState({email: event.target.value}, () => {
            this.checkFormValid();
        });
    };

    handlePasswordChanging = (event) => {
        this.setState({password: event.target.value}, () => {
            this.checkFormValid();
        });
    };

    checkFormValid = () => {
        let {password, email} = this.state;
        if (password && email) {
            this.setState({formIsValid: true});
        } else {
            this.setState({formIsValid: false});
        }
    };

    login = (event) => {
        const {userIsLogged, setUsername} = this.props.actions;
        userIsLogged();
        event.preventDefault();
        let {password, email} = this.state;
        sessionStorage.setItem("email", email);
        axios.post(`/api/login`,
            {
                email: email,
                password: password
            }
        )
            .then((res) => {
                // console.log(typeof res.status);
                if(res.status === 200) {
                    setUsername(res.data.username);
                    localStorage.setItem("username", res.data.username);
                    location.href = "/user-cabinet";
                }
            });
    };


    render() {

        let {email, password, formIsValid, formIsShowing} = this.state;

        return (
            <div>
                <div className="static-modal">
                    <Modal show={formIsShowing}>

                        <Modal.Header>
                            <Modal.Title>LogIn</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Form horizontal onSubmit={this.login}>


                                <FormGroup controlId="formHorizontalEmail">
                                    <Col componentClass={ControlLabel} sm={2}>
                                        Email
                                    </Col>
                                    <Col sm={10}>
                                        <FormControl type="email" placeholder="Email" value={email}
                                                     onChange={this.handleEmailChanging}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup controlId="formHorizontalPassword">
                                    <Col componentClass={ControlLabel} sm={2}>
                                        Password
                                    </Col>
                                    <Col sm={10}>
                                        <FormControl type="password" placeholder="Password" value={password}
                                                     onChange={this.handlePasswordChanging}/>
                                    </Col>
                                </FormGroup>

                                <FormGroup>
                                    <Col smOffset={6} sm={1}>
                                        <Button type="submit" disabled={!formIsValid}>Confirm</Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </Modal.Body>

                    </Modal>
                </div>

            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(null, mapDispatchToProps)(LoginForm);