import React from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

import RegistrationForm from './RegistrationForm';
import LoginForm from './LoginForm';
import {CabinetMain} from '../cabinet/CabinetMain';
import {connect} from "react-redux";
import * as actions from "../../actions/actions";
import {bindActionCreators} from "redux";


const Main = (state) => (
    <BrowserRouter>
        <Switch>
            <Route path='/registration-form' component={RegistrationForm}/>
            <Route path='/login-form' component={LoginForm}/>
            {/*<PrivateRoute path='/user-cabinet' component={CabinetMain} userIsLogged={state.userIsLogged}/>*/}
            <PrivateRoute path='/user-cabinet' component={CabinetMain} userIsLogged={getAuthStateFromStorage()}/>
        </Switch>
    </BrowserRouter>
);

const PrivateRoute = ({component: CabinetMain, ...rest}) => (
    <Route {...rest} render={(props) => (
        rest.userIsLogged === true
            ? <CabinetMain/>
            : <Redirect to='/login-form'/>
    )}/>
);

const getAuthStateFromStorage = () => {
    try {
        const serializedState = sessionStorage.getItem('userIsLogged');

        // debugger;

        if (serializedState === null) {
            return undefined;
        }

        return JSON.parse(serializedState);

    } catch (err) {
        return undefined;
    }
};

const mapStateToProps = (state) => {
    return {
        userIsLogged: state.userIsLogged
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);

