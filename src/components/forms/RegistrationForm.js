import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import axios from 'axios';
import {Form, FormGroup, Col, FormControl, Button, ControlLabel, Modal} from 'react-bootstrap';

import * as actions from "../../actions/actions";


class RegistrationForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            email: '',
            formIsValid: false,
            formIsShowing: true
        };
    }

    handleUsernameChanging = (event) => {
        this.setState({username: event.target.value}, () => {
            this.checkFormValid();
        });
    };

    handlePasswordChanging = (event) => {
        this.setState({password: event.target.value}, () => {
            this.checkFormValid();
        });
    };

    handleEmailChanging = (event) => {
        this.setState({email: event.target.value}, () => {
            this.checkFormValid();
        });
    };

    checkFormValid = () => {
        let {password, username, email} = this.state;
        if (password && username && email) {
            this.setState({formIsValid: true});
        } else {
            this.setState({formIsValid: false});
        }
    };

    getAccess = (event) => {
        const {setUsername} = this.props.actions;
        event.preventDefault();
        let {username, password, email} = this.state;
        axios.post(`/api/signup`, (
            {
                email: email,
                username: username,
                password: password
            }
        ))
            .then((res) => {
                if(res.status === 200) {
                    location.href = "/login-form";
                }
                localStorage.setItem("username", username);
            });
    };


    render() {

        let {username, password, email, formIsValid, formIsShowing} = this.state;

        return (
            <div>

                <div className="static-modal">
                    <Modal show={formIsShowing}>

                        <Modal.Header>
                            <Modal.Title>Registration</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Form horizontal onSubmit={this.getAccess}>
                                <FormGroup controlId="formHorizontalUsername">
                                    <Col componentClass={ControlLabel} sm={2}>
                                        Username
                                    </Col>
                                    <Col sm={10}>
                                        <FormControl type="string" placeholder="Username" value={username}
                                                     onChange={this.handleUsernameChanging}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup controlId="formHorizontalPassword">
                                    <Col componentClass={ControlLabel} sm={2}>
                                        Password
                                    </Col>
                                    <Col sm={10}>
                                        <FormControl type="password" placeholder="Password" value={password}
                                                     onChange={this.handlePasswordChanging}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup controlId="formHorizontalEmail">
                                    <Col componentClass={ControlLabel} sm={2}>
                                        Email
                                    </Col>
                                    <Col sm={10}>
                                        <FormControl type="email" placeholder="Email" value={email}
                                                     onChange={this.handleEmailChanging}/>
                                    </Col>
                                </FormGroup>

                                <FormGroup>
                                    <Col smOffset={6} sm={1}>
                                        <Button type="submit" disabled={!formIsValid}>Confirm</Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </Modal.Body>

                    </Modal>
                </div>

            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(null, mapDispatchToProps)(RegistrationForm);