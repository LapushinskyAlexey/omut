import styled from 'styled-components';


export const ForgotPassword = styled.a`
    font-size: 12px;
    cursor: pointer;
`;

export const ConfirmAccess = ForgotPassword.extend``;