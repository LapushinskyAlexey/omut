import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import configureStore from './store/configureStore';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';


const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);

export const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state.auth.userIsLogged);
        sessionStorage.setItem('userIsLogged', serializedState);
    } catch (err) {
        console.error(err);
    }
};

store.subscribe(() => {
    saveState({
        auth: store.getState("userIsLogged")
    });
});
// registerServiceWorker();
