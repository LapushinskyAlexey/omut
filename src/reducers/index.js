const initialState = {
    /*User*/
    userIsLogged: false,

    /*Forms*/
    confirmFormIsShowing: false,
    forgetPasswordFormIsShowing: false,
    formIsShowing: false,

    /*Cabinet*/
    username: '',
    currentPathname: '/',
    accessToViewAndEdit: false
};

export default function rootReducer(state = initialState, action) {

    switch (action.type) {

        /*User*/

        case 'USER_IS_LOGGED':
            return {...state, userIsLogged: true};

        case 'USER_IS_LOGOUTED':
            return {...state, userIsLogged: false};

        /*Forms*/

        case 'SHOW_CONFIRM_FORM':
            return {...state, confirmFormIsShowing: true};

        case 'CLOSE_CONFIRM_FORM':
            return {...state, confirmFormIsShowing: false};

        case 'SHOW_FORGOT_PASSWORD_FORM':
            return {...state, forgetPasswordFormIsShowing: true};

        case 'CLOSE_FORGOT_PASSWORD_FORM':
            return {...state, forgetPasswordFormIsShowing: false};

        case 'SHOW_FORM':
            return {...state, formIsShowing: true};

        case 'CLOSE_FORM':
            return {...state, formIsShowing: false};

        case 'SET_USERNAME':
            return {...state, username: action.payload};


        /*Cabinet*/

        case 'SET_CURRENT_PATHNAME':
            return {...state, currentPathname: action.payload};

        case 'GET_ACCESS_TO_VIEW_AND_EDIT':
            return {...state, accessToViewAndEdit: true};


        default:
            return state;

    }

};