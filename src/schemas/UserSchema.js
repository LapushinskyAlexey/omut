var mongoose = require('mongoose');
var bcrypt = require('bcrypt');


let UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    }
});

UserSchema.statics.authenticate = (email, password, callback) => {
    User.findOne({ email: email })
        .exec((err, user) => {
            if (err) {
                return callback(err);
            } else if (!user) {
                const err = new Error('User not found.');
                err.status = 401;
                return callback(err);
            }
            bcrypt.compare(password, user.password, (err, result) => {
                if (result === true) {
                    return callback(null, user);
                } else {
                    return callback();
                }
            })
        });
};

UserSchema.pre('save', function (next) {
    const salt = 17;
    let self = this;
    bcrypt.hash(self.password, salt, (err, hash) => {
        if (err) {
            return next(err);
        }
        self.password = hash;
        next();
    })
});

let User = mongoose.model('User', UserSchema);

module.exports = User;