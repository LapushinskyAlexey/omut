const path = require('path');

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devServer: {
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.(jsx|js)?$/,
                include: path.resolve(__dirname, "src"),
                exclude: "/node_modules/",
                loader: "babel-loader"
            },
            {
                test:/\.css$/,
                use:["style-loader","css-loader", "sass-loader"]
            }
        ]
    }
};